package com.hoanphan.recycleviewbasic;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hoanphan.recycleviewbasic.Model.Person;

import java.util.List;

public class RecycleAdapter extends RecyclerView.Adapter<RecycleAdapter.DataViewHolder> {

    private List<Person> people;
    private Context context;

    public RecycleAdapter(List<Person> people, Context context) {
        this.people = people;
        this.context = context;
    }


    @NonNull
    @Override
    public RecycleAdapter.DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;

        // Kiểm tra item view type, nếu là 1 thì inflate layout item_names.xml, 2 thì sử dụng item_names_female.
        switch (viewType) {
            case 1:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name, parent, false);
                break;
            case 2:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name_female, parent, false);
                break;
            default:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name, parent, false);
                break;
        }
        return new DataViewHolder(itemView);
    }

    public int getItemViewType(int position) {
        // Cài đặt kiểu item view type cho từng phần tử, nếu có giới tính là nam thì trả về 1, nữ thì trả về 2.
        if (people.get(position).isMale()) {
            return 1;
        } else {
            return 2;
        }
    }

    @Override
    public void onBindViewHolder(RecycleAdapter.DataViewHolder holder, int position) {
        String name = people.get(position).getmName();
        holder.tvName.setText(name);
    }

    @Override
    public int getItemCount() {
        return people==null ? 0 : people.size();
    }
    public static class DataViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;

        public DataViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.textView);
        }
    }

}
