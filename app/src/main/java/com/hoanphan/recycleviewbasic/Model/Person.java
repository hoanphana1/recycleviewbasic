package com.hoanphan.recycleviewbasic.Model;

public class Person {
    private String mName;
    private boolean isMale;

    public Person(String mName, boolean isMale) {
        this.mName = mName;
        this.isMale = isMale;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }
}
